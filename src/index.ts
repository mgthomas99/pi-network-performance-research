#!/usr/bin/env ts-node

/// <reference path="./../typings/arg-n.d.ts" />

import { measure } from "pi-temperature";
import { measure as m2 } from "./async-temperature";

// const buffer: Array<Arg1Of<typeof measure>> = [ ];

process.stdin.setEncoding("utf8");
process.stdin.resume();

process.stdin.on("data", function (chunk: Buffer | string) {
//   if (buffer.length > 1) {
//     console.warn(`Buffer overflow (${buffer})`);
//   }

//   const callback = <Arg1Of<typeof measure>> function (err, temp) {
//     console.log(`${temp} C`);

//     buffer.splice(index, 1);
//   };

//   const index = buffer.length;
//   buffer.push(callback);
  console.log(`\t${chunk}`);
});

process.stdin.on("end", function () {
  console.info("Stream closed");
});
