import * as native from "pi-temperature";

export function measure () {
  return new Promise<number>(function (accept, reject) {
    native.measure(function (err, temp) {
      return err ? reject(err) : accept(temp);
    });
  });
}
