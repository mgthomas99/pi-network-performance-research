#!/usr/bin/env ts-node

import p from "./utils/stdin-mw";

const suffix = process.argv[2] || "";

export default p(function (chunk) {
  process.stdout.write(chunk);
  process.stdout.write(suffix);
  process.stdout.write("\n");
});
