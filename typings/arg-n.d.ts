
declare type Arg1Of<T> = T extends (arg0: unknown, arg1: infer R) => any ? R : unknown;
