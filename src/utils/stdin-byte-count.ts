#!/usr/bin/env ts-node

process.stdin.setEncoding("utf8");
process.stdin.resume();

let index = 0;

process.stdin.on("data", function (chunk: Buffer | string) {
  const bytes = Buffer.byteLength(chunk);

  console.log(`chunk ${index}`);
});

process.stdin.on("end", function () {
  console.info("Stream closed");
});
