
export type DataStreamCallback =
    (chunk: Buffer | string) => void;

export default function (cb: DataStreamCallback) {
  let $;

  process.stdin.on("data", $ = function (chunk) {
    cb(chunk);
  });

  process.stdin.once("end", function () {
    process.stdin.off("data", $);
  });
}
